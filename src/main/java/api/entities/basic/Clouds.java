package api.entities.basic;

/**
 * Created by dyagilev on 21.03.2017.
 */
public class Clouds {

    private Integer all;
    public Integer getAll() {
        return all;
    }

    public void setAll(Integer all) {
        this.all = all;
    }

    @Override
    public String toString() {
        return "ClassPojo {all = " + all + "}";
    }
}
