package api.entities.basic;

/**
 * Created by dyagilev on 21.03.2017.
 */
public class Coords {

    private Double lat;
    private Double lon;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "ClassPojo {lat = " + lat + "lon = " + lon + "}";
    }
}
