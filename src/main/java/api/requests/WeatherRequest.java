package api.requests;

import api.entities.WeatherParamRQ;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.ValidatableResponse;
import utils.JsonUtils;
import utils.MainReporter;
import com.jayway.restassured.response.Header;
import java.util.Map;

/**
 * Created by dyagilev on 21.03.2017.
 */
public class WeatherRequest {
    private String URL;
    private Header header;
    private Map<String, String> parametersMap;

    public WeatherRequest(String URL, Header header, WeatherParamRQ weatherParamRQ) {
        this.URL = URL;
        this.header = header;
        this.parametersMap = weatherParamRQ.setParameters();
    }

    public Map<String, String> getParametersMap() {
        return parametersMap;
    }

    public void setParametersMap(Map<String, String> parametersMap) {
        this.parametersMap = parametersMap;
    }

    public ValidatableResponse send() {
        ValidatableResponse response = RestAssured.
                given().
                    baseUri(URL).
                    header(header).
                    params(parametersMap).
                when().
                get().
                then();
        MainReporter.logInfo("Weather Request: " + parametersMap.entrySet().toString());
        JsonUtils.jsonToFile("Response", response.extract().asString());
        return response;
    }
}
