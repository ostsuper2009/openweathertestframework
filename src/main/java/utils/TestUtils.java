package utils;

import java.io.UnsupportedEncodingException;

/**
 * Created by dyagilev on 20.03.2017.
 */
public class TestUtils {

    public static String convertToUTF8(String string) {
        String stringUtf8 = null;
        try {
            byte text[] = string.getBytes("UTF-8");
            stringUtf8 = new String(text, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            MainReporter.logException(e.getMessage());
        }
        return stringUtf8;
    }
}
