package utils;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.Assert;
import ui.ConfigEntity;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.io.File;
import java.util.logging.Level;

/**
 * Created by dyagilev on 14.03.2017.
 */
public class WebDriverFactory {

    public static final String FIREFOX = "firefox";
    public static final String CHROME = "chrome";
    public static final String IE = "ie";
    public static final String SAFARY = "";

    private static WebDriver driver;

    private static WebDriverWrapper driverWrapper;
    private static ConfigEntity param;

    public static WebDriverWrapper initDriver(String browser) {
        Dimension dimension = new Dimension(1920, 1080);

        if (browser.equals(FIREFOX)) {
            DesiredCapabilities firefoxCap = setDefaultCapabilitiesForFirefox();
            driver = new FirefoxDriver(firefoxCap);
            driver.manage().window().maximize();
            driver.manage().window().setSize(dimension);
            driverWrapper = new WebDriverWrapper(driver);
        } else if (browser.equals(CHROME)) {
            param = new ConfigEntity();
            param = YamlFileReader.fromYaml(ConfigEntity.class, System.getProperty("user.dir") + "/src/test/config/webDriverConfig.yml");

            File chromeDriverPath = new File(param.getChromeDriverPath());

            if (chromeDriverPath.exists()) {
                MainReporter.logInfo("Set property webdriver.chrome.driver = '" + chromeDriverPath.getAbsolutePath() + "'");
                System.setProperty("webdriver.chrome.driver", chromeDriverPath.getAbsolutePath());
            }

            DesiredCapabilities chromeCap = setDefaultCapabilitiesForChrome();
            driver = new ChromeDriver(chromeCap);
            driver.manage().window().maximize();
            driverWrapper = new WebDriverWrapper(driver);

        } else if (browser.equals(IE)) {
            param = new ConfigEntity();
            param = YamlFileReader.fromYaml(ConfigEntity.class, System.getProperty("user.dir") + "/src/test/config/webDriverConfig.yml");

            File ieDriverPath = new File(param.getIeDriverPath());

            if (ieDriverPath.exists()) {
                System.setProperty("webdriver.ie.driver", ieDriverPath.getAbsolutePath());
            }

            DesiredCapabilities ieCap = setDefaultCapabilitiesForIe();
            driver = new InternetExplorerDriver(ieCap);
            driver.manage().window().maximize();
            driverWrapper = new WebDriverWrapper(driver);

//        } else if (browser.equals(SAFARY)) {
//            driver = new SafariDriver();
//            driverWrapper = new WebDriverWrapper(driver);
        }

        else {
            Assert.fail("No information about launching '" + browser + "' browser!!!");
        }

        return driverWrapper;
    }

    public static WebDriverWrapper getDriverWrapper() {
        return driverWrapper;
    }

    private static DesiredCapabilities setDefaultCapabilitiesForFirefox() {
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setEnableNativeEvents(true);
        DesiredCapabilities firefoxCapabilities = DesiredCapabilities.firefox();
        firefoxCapabilities.setCapability(FirefoxDriver.PROFILE, firefoxCapabilities);

        return firefoxCapabilities;
    }

    private static DesiredCapabilities setDefaultCapabilitiesForChrome() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
//      options.addArguments("user-data-dir=D:\\UserDataDir");
//      options.addArguments("-incognito");
        options.addArguments("--no-sandbox");

        DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
        LoggingPreferences loggingPreferences = new LoggingPreferences();
        loggingPreferences.enable(LogType.BROWSER, Level.ALL);
        chromeCapabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingPreferences);
        chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
        chromeCapabilities.setCapability("ignoreZoomSetting", true);

        return chromeCapabilities;
    }

    private static DesiredCapabilities setDefaultCapabilitiesForIe() {

        DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.ALL);
        ieCapabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        ieCapabilities.setCapability("ignoreZoomSetting", true);

        return ieCapabilities;
    }

}
