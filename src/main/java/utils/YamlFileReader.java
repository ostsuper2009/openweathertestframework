package utils;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import java.io.*;


/**
 * Created by dyagilev on 14.03.2017.
 */
public class YamlFileReader<T> {

    public static <T> T fromYaml(Class<T> className, String filePath) {

        File file = new File(filePath);
        T obj = null;
        try {
            YamlReader reader = new YamlReader(new FileReader(file.getAbsolutePath()));
            obj = reader.read(className);
        } catch (FileNotFoundException e) {
            MainReporter.logException("File Not Found" + e.getMessage());
        } catch (YamlException e) {
            MainReporter.logException("Yaml exception " + e.getMessage());
        }
        return obj;
    }
}
