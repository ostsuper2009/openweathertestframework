package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by dyagilev on 14.03.2017.
 */
public class DateAndTimeHelper {

    public static String getDateTime(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, days);
        calendar.set(Calendar.SECOND, 0);
        Date date = calendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String dateNewFormat = dateFormat.format(date);

        return dateNewFormat;
    }

    public static String getDateForFileName() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss-SSS");
        String dateNewFormat = dateFormat.format(date);

        return dateNewFormat;
    }

    public static String formatDate(String dateTime, String formatIn, String formatOut) {
        String dateNewFormat = null;
        SimpleDateFormat baseDateFormat = new SimpleDateFormat(formatIn);
        SimpleDateFormat newFormat = new SimpleDateFormat(formatOut, Locale.ENGLISH);
        try {
            Date date = baseDateFormat.parse(dateTime);
            dateNewFormat = newFormat.format(date);

        } catch (ParseException e) {
            MainReporter.logException("ParseException: " + e.getMessage());
        }

        return dateNewFormat;
    }

    public static String getDateTime(int days, String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, days);
        calendar.set(Calendar.SECOND, 0);
        Date date = calendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        String dateNewFormat = dateFormat.format(date);

        return dateNewFormat;
    }

    public static String getHourBackTime(int hours, String format) {
        Date date = new Date(System.currentTimeMillis()- TimeUnit.HOURS.toMillis(hours));
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        String dateNewFormat = dateFormat.format(date);

        return dateNewFormat;
    }

    public static String getHourAndMinuteForwardTime(int hours, String format) {
        Date date = new Date(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(hours) + TimeUnit.MINUTES.toMillis(1));
        SimpleDateFormat isoFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        String dateNewFormat = isoFormat.format(date);

        return dateNewFormat;
    }

    public static String getDateTime(String formatOut) {
        Date date = new Date();
        SimpleDateFormat isoFormat = new SimpleDateFormat(formatOut, Locale.ENGLISH);
        String dateNewFormat = isoFormat.format(date);

        return dateNewFormat;
    }

    private static Date getDate(String time) {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), Integer.valueOf(time.substring(0, 2)), Integer.valueOf(time.substring(3, 5)));

        return cal.getTime();
    }

    public static boolean compareDateWithCurrent_1min_accuracy(String actualDate) {
        long actualDateLong = getDate(actualDate).getTime();
        long expectedDateLong = (new Date()).getTime();
        long diff = Math.abs(expectedDateLong - actualDateLong);

        if(diff > 60000) {
            MainReporter.logError("More than 1 minute between dates");
            return false;
        }

        return true;
    }


}
