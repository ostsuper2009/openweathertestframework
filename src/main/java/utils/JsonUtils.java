package utils;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.map.ObjectMapper;
import java.io.File;
import java.io.IOException;

/**
 * Created by dyagilev on 21.03.2017.
 */
public class JsonUtils {
    public static String objectToJson(Object object) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        return json;
    }

    public static void jsonToFile(String type, String json) {
        String fileName = "/" + type + "_" + DateAndTimeHelper.getDateForFileName();

        File dir = new File(System.getProperty("user.dir") + "/target/surefire-reports/html/request_responses");
        if (!dir.exists()) dir.mkdirs();

        try {
            ObjectMapper mapper = new ObjectMapper();
            Object jsonRS = mapper.readValue(json, Object.class);
            String jsonPretty = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonRS);

            File newTextFile = new File(dir + fileName + ".html");
            MainReporter.logJsonFile(type, "request_responses" + fileName + ".html");
            FileUtils.writeStringToFile(newTextFile, jsonPretty);
        } catch (IOException e1) {
            try {
                File newTextFile = new File(dir + fileName + ".html");
                MainReporter.logHtmlFile(type, "request_responses" + fileName + "");
                FileUtils.writeStringToFile(newTextFile, json);
            } catch (IOException e2) {}
            }
    }
}
