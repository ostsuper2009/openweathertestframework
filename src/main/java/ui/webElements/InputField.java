package ui.webElements;

import org.openqa.selenium.By;
import utils.TestUtils;

/**
 * Created by dyagilev on 20.03.2017.
 */
public class InputField extends BaseElement {
    public InputField() {
        super();
    }

    public void senKeys(By by, CharSequence... charSequences) {
        driver.findElement(by).sendKeys(charSequences);
    }

    public void clear(By by) {
        driver.findElement(by).clear();
        //FOR IE
        if (driver.findElement(by).getAttribute("value").equalsIgnoreCase("")) {
            driver.findElement(by).clear();
        }
    }

    @Override
    public String getText(By by) {
        return TestUtils.convertToUTF8(driver.findElement(by).getAttribute("value"));
    }

    public String getPlaceholderValue(By by) {
        return driver.findElement(by).getAttribute("placeholder");
    }
}
