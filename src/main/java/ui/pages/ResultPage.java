package ui.pages;

import com.fasterxml.jackson.databind.jsontype.impl.StdTypeResolverBuilder;
import org.openqa.selenium.By;
import utils.MainReporter;

/**
 * Created by dyagilev on 20.03.2017.
 */
public class ResultPage extends BasePage {


    private static final By RESULT_CITY = By.xpath("//table/tbody/tr/td[2]/b[1]/a");

    public static By getResultCity() {
        return RESULT_CITY;
    }

    public boolean verifySearchRequest(HomePage homePage) {
        String cityExpected = homePage.getCity();

        String cityCountryActual = baseElement.getText(RESULT_CITY);
        String cityActual = cityCountryActual.substring(0, cityCountryActual.indexOf(','));
        if (!cityActual.equalsIgnoreCase(cityExpected)) {
            MainReporter.logError("cityExpected: " + cityExpected);
            MainReporter.logError("cityActual" + cityActual);
            return false;
        }
        return true;
    }
}
