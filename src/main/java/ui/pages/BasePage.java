package ui.pages;

import ui.webElements.BaseElement;
import ui.webElements.DropDown;
import ui.webElements.InputField;
import utils.MainReporter;
import utils.WebDriverFactory;
import utils.WebDriverWrapper;

/**
 * Created by dyagilev on 17.03.2017.
 */
public abstract class BasePage {
    protected String pageUrl;
    protected String basePageUrl;
    protected WebDriverWrapper driver;

    protected InputField inputField;
    protected BaseElement baseElement;
    protected DropDown dropDown;

    public boolean isOpened() {
        MainReporter.logInfo("'" + pageUrl + "' is opened");
        return driver.getCurrentUrl().contains(pageUrl);
    }

    public BasePage() {
        driver = WebDriverFactory.getDriverWrapper();
        setElements();
    }

    public BasePage(String basePageUrl) {
        driver = WebDriverFactory.getDriverWrapper();
        this.basePageUrl = basePageUrl;
        pageUrl = basePageUrl;
        setElements();
    }

    private void setElements(){
        inputField = new InputField();
        baseElement = new BaseElement();
        dropDown = new DropDown();
    }
}
