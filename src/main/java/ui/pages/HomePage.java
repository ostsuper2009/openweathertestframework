package ui.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.DateAndTimeHelper;
import utils.MainReporter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HomePage extends BasePage {

    private static final By SEARCH_INPUT = By.xpath("//*[@placeholder='Your city name']");
    private static final By SEARCH_BUTTON = By.xpath("//button[@class='btn btn-orange' and @type='submit']");
    private static final By ALL_LINKS = By.xpath("//*[@href]");

    private String date;
    private String city;

    public HomePage(String url) {
        super(url);
    }

    public static By getSearchInput() {
        return SEARCH_INPUT;
    }

    public static By getSearchButton() {
        return SEARCH_BUTTON;
    }

    public static By getAllLinks() {
        return ALL_LINKS;
    }

    public static String isLinkBroken(URL url) throws IOException {
        String response = "";
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            connection.connect();
            response = connection.getResponseMessage();
            connection.disconnect();

            return response;
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void open() {
        MainReporter.logInfo("Open openweathermap.org.");
        driver.get(pageUrl);
        driver.waitUrlLoaded();
        MainReporter.logUrl(driver.getCurrentUrl());
    }

    public void fillSearch(String city) {
        MainReporter.logInfo("Fill search form : " + city);
        inputField.click(SEARCH_INPUT);
        inputField.clear(SEARCH_INPUT);
        inputField.senKeys(SEARCH_INPUT, city);
        setSearchParameters(city);
    }

    public void setSearchParameters(String cityData) {
        this.city = cityData;
        this.date = DateAndTimeHelper.getDateTime(0);
    }

    public void search() {
        MainReporter.logTestStep("Search.");
        baseElement.click(SEARCH_BUTTON);
        baseElement.waitUntilInvisibilityOfElement(ResultPage.getResultCity());
    }

    public List getListOfAllLinks() {
        List<WebElement> list = new ArrayList();
        list = driver.findElements(By.tagName("a"));
        list.addAll(driver.findElements(By.tagName("img")));

        List finalList = new ArrayList();
        for (WebElement element: list) {
            if (element.getAttribute("href") != null) {
                finalList.add(element);
            }
        }
        return finalList;
    }

}
