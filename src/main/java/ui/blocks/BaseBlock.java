package ui.blocks;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import ui.webElements.BaseElement;
import ui.webElements.DropDown;
import ui.webElements.InputField;
import utils.WebDriverFactory;
import utils.WebDriverWrapper;

/**
 * Created by dyagilev on 20.03.2017.
 */
public class BaseBlock {

    protected WebDriverWrapper driver;

    protected InputField inputField;
    protected BaseElement baseElement;
    protected DropDown dropDown;

    public BaseBlock() {
        driver = WebDriverFactory.getDriverWrapper();
        setElements();
    }

    private void setElements() {
        inputField = new InputField();
        baseElement = new BaseElement();
        dropDown = new DropDown();
    }

    public void refreshPage(By by) {
        driver.findElement(by).sendKeys(Keys.F5);
    }
}
