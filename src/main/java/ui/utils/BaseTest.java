package ui.utils;


import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import ui.webElements.BaseElement;
import utils.MainReporter;
import utils.WebDriverFactory;


/**
 * Created by dyagilev on 21.03.2017.
 */
public class BaseTest extends BaseElement {
    protected static String URL;
    protected static String environment;
    protected static String browser;
    protected static String branchName;

    @BeforeSuite(groups = {"Config"})
    public void setUp() {
        PropertyConfigurator.configure("src/main/resources/log4j.properties");
        WebDriverFactory webDriverFactory = new WebDriverFactory();
        getProperties();
        driver = webDriverFactory.initDriver(browser);
        setURL();

    }

    public void getProperties() {
        browser = System.getProperty("browser");
        environment = System.getProperty("environment");
        branchName = System.getProperty("branchName");

        if (browser == null) {
            browser = "chrome";
        }

        if (environment == null) {
            environment = "production";
        }

        if (branchName == null) {
            branchName = "master";
        }

        MainReporter.logInfo("---Parameters---");
        MainReporter.logInfo("Browser: '" + browser + "'");
        MainReporter.logInfo("Environment: '" + environment + "'");
        MainReporter.logInfo("BranchName: '" + branchName + "'");
        MainReporter.logInfo("---Parameters---");
    }

    private void setURL() {
        if (environment.equalsIgnoreCase("production")) {
            URL = "http://openweathermap.org/";
        }
    }

    @AfterSuite(groups = {"Config"})
    public void quitDriver() {
        driver.quit();
    }

}
