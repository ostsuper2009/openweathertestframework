package ui;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import ui.blocks.Header;
import ui.pages.HomePage;
import ui.pages.ResultPage;
import ui.utils.BaseTest;
import utils.MainReporter;

import java.net.URL;
import java.util.List;


public class HomePageTest extends BaseTest {

    @Test
    public void testCurrentLocation() {
        HomePage homePage = new HomePage(URL);
        ResultPage resultPage = new ResultPage();
        Header header = new Header();
        homePage.open();
        homePage.fillSearch("Kyiv");
        homePage.search();

        Assert.assertTrue(resultPage.verifySearchRequest(homePage));
        header.goToHomePage();
    }

    @Test
    public void testLinksOnHomePage() {
        HomePage homePage = new HomePage(URL);
        homePage.open();
        List<WebElement> list = homePage.getListOfAllLinks();
        MainReporter.logInfo("Total number of links on HomePage is: '" + list.size() + "'.");
        for (WebElement element: list) {
            try {
                String response = HomePage.isLinkBroken(new URL(element.getAttribute("href")));
                MainReporter.logInfo("URL: " + element.getAttribute("href") + " returned: " + response);
                Assert.assertEquals(response, "OK");
            } catch (Exception e) {
                MainReporter.logError("At " + element.getAttribute("innerHTML") + " Exception occurred: " + e.getMessage());
            }
        }
    }

}
